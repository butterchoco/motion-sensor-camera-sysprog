
from picamera import PiCamera
from time import sleep
from datetime import datetime
import RPi.GPIO as GPIO

TIME_FORMAT = "%Y-%M-%d %H-%M-%S"
PHOTO_PATH = "/motion-sensor-camera-sysprog/motion-camera-webserver/photos/"

GPIO.setmode(GPIO.BCM)

P = PiCamera()
P.resolution = (1024, 768)
P.start_preview()

GPIO.setup(23, GPIO.IN)

while True:
    if GPIO.input(23):
        print("Motion...")
        # camera warm-up time
        sleep(2)
        P.capture(f'{PHOTO_PATH}{datetime.now().strftime(TIME_FORMAT)}.JPG')
        sleep(10)
