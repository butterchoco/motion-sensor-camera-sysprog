#!/bin/bash

TARGET=/home/pi/motion-sensor-camera-sysprog/motion-camera-webserver/photos/

inotifywait -m -e create -e moved_to --format "%f" $TARGET \
        | while read FILENAME
                do
                        echo Detected $FILENAME, Uploading
			/home/pi/motion-sensor-camera-sysprog/System/Dropbox-Uploader/dropbox_uploader.sh upload /home/pi/motion-sensor-camera-sysprog/motion-camera-webserver/photos/$FILENAME /media/photos
                done
