# Motion Sensor Camera Project

This is a motion sensor camera project using Raspberry Pi 3 for System Programming Course.
It is developed by "Sysprog Nih Sys" whose members are:

- Ahmad Supriyanto
- Ardanto Finkan
- Feril Bagus
- I Gusti Putu Agastya
- Kevin Raikhan
- Mika Dabelza
