from flask import Flask, request, send_from_directory, render_template, Response
from buttonServo import GPIOClassifier, buttonClassifier, servoClassifier
import subprocess
import socket 
import io
import cv2
import RPi.GPIO as GPIO
from datetime import datetime
import threading
import os
from time import sleep

arah = 7.5
app = Flask(__name__, static_url_path='')


TIME_FORMAT = "%Y-%M-%d_%H-%m-%S"
frame = None
camera = cv2.VideoCapture(0)  # use 0 for web camera
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN)
GPIO.setup(27,GPIO.OUT)
servo = servoClassifier(4)
servo.Setup()
button1 = buttonClassifier(17)
button1.Setup()

def gen_frames():  # generate frame by frame from camera
    while True:
        nf = frame.copy()
        if GPIO.input(23):
			GPIO.output(18,GPIO.HIGH)
            cv2.putText(nf,"Motion Detected!", (30,60), cv2.FONT_HERSHEY_TRIPLEX, 2, (0,255,0))
		else:
			GPIO.output(18,GPIO.LOW)
        ret, buffer = cv2.imencode('.jpg', nf)
        framed = buffer.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + framed + b'\r\n')  # concat frame one by one and show result


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen_frames(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


def take_video():
    while True:
        success, ret = camera.read()
        global frame
        frame = ret

def write_image():
    print("sini")
    while True:
        if GPIO.input(23):
            path = os.path.join("/home/pi/motion-sensor-camera-sysprog/motion-camera-webserver/photos", datetime.now().strftime(TIME_FORMAT) + ".jpg")
            print("sini")
            cv2 .imwrite(path, frame)
            sleep(2)

@app.route('/')
def home():
    return render_template('layout.html')

@app.route('/photos/<file_name>')
def see_photos(file_name):
	return send_from_directory('photos', file_name)

@app.route('/photos')
def photos():
	tampilkan = '<h2>Video Page</h2><ul>'
	result = subprocess.run(['ls', '-1', 'photos'], stdout=subprocess.PIPE).stdout
	file_list = result.decode('utf-8').split('\n')

	del(file_list[-1])

	for file_name in file_list:
		tampilkan += '<li>'
		tampilkan += file_name + '<br>'
		tampilkan += '<img src="photos/{}" style="width: 250px; height: auto">'.format(file_name)
		#tampilkan += '</a>'
		tampilkan += '</li>'
	tampilkan += '</ul>'

	context = file_list
	return render_template('photopage.html', context=context)

@app.route('/control/ubah/<angle>')
def pindah(angle):
	result = subprocess.run(['python3', 'ubahServo.py', angle], stdout=subprocess.PIPE).stdout
	return result

@app.route('/control/right')
def servo_right():
	servo.SetAngle(12.5)
	# subprocess.run(['python3', 'ubahServo.py', "12.5"], stdout=subprocess.PIPE).stdout
	# servo logic
	# servo_driver.kanan()
	return ('', 204)

@app.route('/control/left')
def servo_left():
	servo.SetAngle(2.5)
	# subprocess.run(['python3', 'ubahServo.py', "2.5"], stdout=subprocess.PIPE).stdout

	# servo logic
	# servo_driver.kiri()
	return ('', 204)

@app.route('/control/reset')
def servo_reset():
	servo.SetAngle(7.5)
	# subprocess.run(['python3', 'ubahServo.py', "7.5"], stdout=subprocess.PIPE).stdout

	# servo logic
	# servo_driver.kiri()
	return ('', 204)

def buttonPushed(self):
	print("button pushed.")
	servo.SetAngle(7.5)

def buttonListening():
	print("Button Listening...")
	GPIO.add_event_detect(button1.buttonPIN, GPIO.RISING, callback=buttonPushed)

if __name__ == '__main__':
	thread0 = threading.Thread(target=take_video)
	thread0.start()
	thread1 = threading.Thread(target=write_image)
	thread1.start()
	thread2 = threading.Thread(target=app.run, args=('0.0.0.0', 80))
	thread2.start()
	thread3 = threading.Thread(target=buttonListening)
	thread3.start()
