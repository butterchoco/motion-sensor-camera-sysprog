import RPi.GPIO as GPIO
from time import sleep

class GPIOClassifier:
	def __init__(self):
		GPIO.setwarnings(False)
		GPIO.setmode(GPIO.BCM)

class buttonClassifier:
	def __init__(self, PIN):
		self.buttonPIN = PIN

	def Setup(self):
		GPIO.setup(self.buttonPIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

class servoClassifier:
	def __init__(self, PIN):
		self.servoPIN = PIN
		self.angle = 0
		self.pwm = ""

	def Setup(self):
		GPIO.setup(self.servoPIN, GPIO.OUT)
		self.pwm = GPIO.PWM(self.servoPIN, 50)
		

	def SetAngle(self, angle):
		self.pwm.start(angle)

	def Clean(self):
		self.SetAngle(0)
		self.pwm.stop()
